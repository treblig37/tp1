using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBougant : MonoBehaviour
{
    [SerializeField] float m_MinTime;
    [SerializeField] float m_MaxTime;
    [SerializeField] float m_TimeAfterMove = 0; 
    [SerializeField] float m_TimeMoving;
    [SerializeField] bool m_Boucle = true;
    [SerializeField] Vector3 m_Movement;

    Rigidbody m_Rb;
    Vector3 m_InitialLocation;


    void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
        m_InitialLocation = transform.position;
        StartCoroutine(Delay());
    }

    //fait bouger le cube durant un delay pour ensuite revenir a sont point
    IEnumerator Delay()
    {
        while(m_Boucle)
        {
            float delay = Random.Range(m_MinTime, m_MaxTime);
            yield return new WaitForSeconds(delay);
            m_Rb.velocity = m_Movement;
            yield return new WaitForSeconds(m_TimeMoving);
            m_Rb.velocity = new Vector3(0, 0, 0);
            yield return new WaitForSeconds(m_TimeAfterMove);
            m_Rb.velocity = -m_Movement;
            yield return new WaitForSeconds(m_TimeMoving);
            m_Rb.velocity = new Vector3(0, 0, 0);
            transform.position = m_InitialLocation;
        }
    }
}
