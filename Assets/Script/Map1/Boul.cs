using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boul : MonoBehaviour
{
    [SerializeField] Transform m_Spawn;
    [SerializeField] float m_MinTimeRespawn;
    [SerializeField] float m_MaxTimeRespawn;

    Rigidbody m_Rb;

    void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
        transform.position = m_Spawn.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "BoulRespawn")
        {
            StartCoroutine(respawn());
        }
    }

    //fait respawn la ball a avec un delay aleatoire a l'endroit indiquer
    IEnumerator respawn()
    {
        float delay = Random.Range(m_MinTimeRespawn, m_MaxTimeRespawn);
        yield return new WaitForSeconds(delay);
        m_Rb.velocity = Vector3.zero;
        transform.position = m_Spawn.transform.position;
    }
}
