using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] Transform m_PlayerTransform;
    [SerializeField] float m_Debut;
    [SerializeField] float m_LongeurPiece;

    bool m_OnebuttonClick = false;
    bool m_OneWallPass = false;

    float m_CurrPos;

    void Start()
    {
        m_CurrPos = m_Debut + m_LongeurPiece;    
    }

    void Update()
    {
        SetButtonClick();
    }

    //Set si un boutom est clicker ou sinon pour pouvoir agir dans le code en consquence
    void SetButtonClick()
    {
        if(m_OnebuttonClick == true)
        {
            if(m_PlayerTransform.position.x > m_CurrPos)
            {
                m_OnebuttonClick = false;
                m_OneWallPass = true;
                m_CurrPos += m_LongeurPiece;
            }
        }
    }

    public void SetButtonClick(bool isClick)
    {
        m_OnebuttonClick = isClick;
    }

    public bool CheckButtonClick()
    {
        return m_OnebuttonClick;
    }

    public void SetOneWallPass(bool pass)
    {
        m_OneWallPass = pass;
    }

    public bool CheckOneWallPass()
    {
        return m_OneWallPass;
    }
}
