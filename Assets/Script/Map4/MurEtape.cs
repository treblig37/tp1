using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurEtape : MonoBehaviour
{
    [SerializeField] ButtonManager m_ButtonManager;
    [SerializeField] List<Bouton> m_Boutons;
    [SerializeField] float m_Speed;
    [SerializeField] float m_Distance;
    [SerializeField] float m_Delay;

    bool m_Pass = false;
    bool m_IsSet = false;
    bool m_BoolDelay = false;
    bool m_OneTime = false;

    float m_TargetPosition;
    float m_FirstLocation;

    void Start()
    {
        m_TargetPosition = transform.position.y - m_Distance;
        m_FirstLocation = transform.position.y;
    }

    
    void Update()
    {
        CheckButton();
        SetPosition();
        SetInitialPos();
    }

    //regarde si un bouton est appuyer
    void CheckButton()
    {
        if (!m_Pass)
        {
            for (int i = 0; i < m_Boutons.Count; i++)
            {
                if (m_Boutons[i].CheckButtonPress())
                {
                    m_Pass = true;
                }
            }
        }
    }

    //fait descnedre le mur
    void SetPosition()
    {
        if (m_Pass && !m_IsSet)
        {
            Vector3 position = transform.position;
            position.y -= m_Speed * Time.deltaTime;
            transform.position = position;

            if (position.y < m_TargetPosition)
            {
                position.y = m_TargetPosition;
                transform.position = position;
            }
            if (position.y == m_TargetPosition)
            {
                m_IsSet = true;
                m_Pass = false;
                m_BoolDelay = true;
            }

        }
    }

    //fait remonter le mur
    void SetInitialPos()
    {
        if(m_IsSet && transform.position.y != m_FirstLocation && m_ButtonManager.CheckOneWallPass())
        {
            if(m_BoolDelay && !m_OneTime)
            {
                StartCoroutine(Delay());
                m_OneTime = true;
            }
            else if(!m_BoolDelay)
            {
                Vector3 position = transform.position;
                position.y += m_Speed * Time.deltaTime;
                transform.position = position;

                if (position.y > m_FirstLocation)
                {
                    position.y = m_FirstLocation;
                    transform.position = position;
                }
                if (position.y == m_FirstLocation)
                {
                    m_IsSet = false;
                    m_OneTime = false;
                    m_ButtonManager.SetOneWallPass(false);
                }
            }
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(m_Delay);
        m_BoolDelay = false;
    }
}

