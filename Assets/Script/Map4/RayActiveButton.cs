using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayActiveButton : MonoBehaviour
{
    [SerializeField] Transform m_RayPos;
    [SerializeField] Vector3 m_RayDirection;
    [SerializeField] float m_Distance;

    void Start()
    {
        
    }

    void Update()
    {
        CheckForward();
    }

    void CheckForward()
    {
        Debug.DrawRay(m_RayPos.position, m_RayDirection * m_Distance, Color.red);
        Ray ray = new Ray(m_RayPos.position, m_RayDirection);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, m_Distance))
        {
            if(hitInfo.collider.GetComponent<Bouton>() != null)
            {
                hitInfo.collider.GetComponent<Bouton>().ButtonPress();
            }
        }
    }
}
