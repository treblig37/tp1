using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouton : MonoBehaviour
{
    [SerializeField] ButtonManager m_ButtonManager;
    [SerializeField] float m_DistanceEnfonce;
    [SerializeField] float m_SpeedPress;

    bool m_ButtonPress = false;
    bool m_IsSet = false;
    float m_TargetPosition;

    void Start()
    {
        m_TargetPosition = transform.position.x + m_DistanceEnfonce;
    }

    void Update()
    {
        SetButton();
    }

    //fait enfoncer le bouton
    void SetButton()
    {
        if(m_ButtonPress && !m_IsSet)
        {
            Vector3 position = transform.position;
            position.x += m_SpeedPress * Time.deltaTime;
            transform.position = position;
            if(position.x > m_TargetPosition)
            {
                position.x = m_TargetPosition;
                transform.position = position;
            }
            if(position.x == m_TargetPosition)
            {
                m_IsSet = true;
            }
        }
    }

    public bool CheckButtonPress()
    { 
        return m_ButtonPress;
    }

    //Jai changer cette methode pour utiliser un raycast
    //void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.other.CompareTag("Player"))
    //    {
    //        ButtonPress();
    //    }
    //}

    public void ButtonPress()
    {
        if(!m_ButtonManager.CheckButtonClick())
        {
            m_ButtonPress = true;
            m_ButtonManager.SetButtonClick(true);
        }
    }
}
