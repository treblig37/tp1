using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurPick : MonoBehaviour
{
    [SerializeField] List<Bouton> m_Boutons;
    [SerializeField] float m_Speed;
    [SerializeField] float m_Distance;
    [SerializeField] bool m_Right;

    bool m_Kill = false;
    bool m_IsSet = false;

    float m_TargetPosition;

    void Start()
    {
        m_TargetPosition = transform.position.z + m_Distance;
    }
    
    void Update()
    {
        CheckButton();
        SetPosition();
    }

    //regarde si un bouton est appuyer
    void CheckButton()
    {
        if(!m_Kill)
        {
            for(int i = 0; i < m_Boutons.Count; i++)
            {
                if(m_Boutons[i].CheckButtonPress())
                {
                    m_Kill = true;
                }
            }
        }
    }

    //si le bouton est appuyer fait refermer le mur
    void SetPosition()
    {
        if(m_Kill && !m_IsSet)
        {
            Vector3 position = transform.position;
            position.z += m_Speed * Time.deltaTime;
            transform.position = position;
            
            if(m_Right)
            {
                if (position.z > m_TargetPosition)
                {
                    position.z = m_TargetPosition;
                    transform.position = position;
                }
                if (position.z == m_TargetPosition)
                {
                    m_IsSet = true;
                }
            }
            else
            {
                if (position.z < m_TargetPosition)
                {
                    position.z = m_TargetPosition;
                    transform.position = position;
                }
                if (position.z == m_TargetPosition)
                {
                    m_IsSet = true;
                }
            }
        }
    }
}
