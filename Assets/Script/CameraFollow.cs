using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform m_PointFollow;
    [SerializeField] Transform m_CamTransfrom;
    [SerializeField] float m_Offset;
    [SerializeField] float m_SensibiliterX = 2.0f;
    [SerializeField] float m_SensibiliterY = 2.0f;
    [SerializeField] float m_MaxAngleX = 60.0f;
    [SerializeField] float m_MinAngleX = -10.0f;
    [SerializeField] float m_FirstAngleY = 0.0f;


    float m_AngleX = 0.0f;
    float m_AngleY = 0.0f;

    void Start()
    {
        m_AngleY = m_FirstAngleY;
        SetPosition();
        SetCamFirstPositionAndRotation();
    }

    void Update()
    {
        SetPosition();
        SetMouseRotation();
    }

    void SetPosition()
    {
        transform.position = m_PointFollow.position;
    }

    //fait = la position de la camera a celle du joueur
    void SetCamFirstPositionAndRotation()
    {
        m_CamTransfrom.position = new Vector3(m_PointFollow.position.x, m_PointFollow.position.y, m_PointFollow.position.z + m_Offset);
        transform.rotation = m_PointFollow.rotation;
    }

    //prend les input de la souris et les applique a la camera
    void SetMouseRotation()
    {
        m_AngleY += m_SensibiliterX * Input.GetAxis("Mouse X");
        m_AngleX += m_SensibiliterY * Input.GetAxis("Mouse Y");

        if (m_AngleX < m_MinAngleX)
        {
            m_AngleX = m_MinAngleX;
        }
        else if (m_AngleX > m_MaxAngleX)
        {
            m_AngleX = m_MaxAngleX;
        }

        transform.eulerAngles = new Vector3(m_AngleX, m_AngleY, 0.0f);

    }


}
