using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] float m_MinY;
    [SerializeField] float m_DelayChangeScene;
    [SerializeField] float m_DelayDie;
    [SerializeField] HUD m_Hud;

    bool m_IsDeath = false;
    bool m_Finish = false;
    bool m_NoOther = false;

    void Start()
    {
        
    }

    
    void Update()
    {
        LoadCurrScene();
        LoadNextScene();
    }

    public void SetIsDeath(bool isDeath)
    {
        m_IsDeath = isDeath;
    }

    public void SetIsFinish(bool finish)
    {
        m_Finish = finish;
    }

    public bool CheckIsDeath()
    {
        return m_IsDeath;
    }

    public float CheckMinY()
    {
        return m_MinY;
    }

    void LoadCurrScene()
    {
        if(m_IsDeath && !m_NoOther)
        {
            StartCoroutine(LoadScene(SceneManager.GetActiveScene().buildIndex, m_DelayDie));
            m_IsDeath = false;
            m_NoOther = true;
        }
    }

    void LoadNextScene()
    {
        if(m_Finish && !m_NoOther)
        {
            if(SceneManager.GetActiveScene().buildIndex == 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else if(SceneManager.GetActiveScene().buildIndex != SceneManager.sceneCountInBuildSettings - 1)
            {
                m_Hud.SetScore(m_Hud.GetCurrScore());
                StartCoroutine(LoadScene(SceneManager.GetActiveScene().buildIndex + 1, m_DelayChangeScene));
            }
            else
            {
                if(m_Hud.GetCurrScore() > PlayerPrefs.GetInt("MaxScore"))
                {
                    PlayerPrefs.SetInt("MaxScore", m_Hud.GetCurrScore());
                }
                StartCoroutine(LoadScene(0, m_DelayChangeScene));
            }
            m_Finish = false;
            m_NoOther = true;
        }
    }

    IEnumerator LoadScene(int scene, float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(scene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
