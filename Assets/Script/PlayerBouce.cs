using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBouce : MonoBehaviour
{
    [SerializeField] float m_ForceBouce = 5;

    Rigidbody m_Rb;
    Vector3 m_Velo;

    void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bouce")
        {
            Bouce();
        }
    }

    void Bouce()
    {
        m_Velo = m_Rb.velocity;
        m_Velo.y = m_ForceBouce;
        m_Rb.velocity = m_Velo;
    }
}
