using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] Text m_ScoreText;
    [SerializeField] Text m_SpeedText;

    static int m_Score = 0;
    static int m_CurrScore = 0;

    int m_MoveSpeed = 0;

    void Start()
    {
        if(!PlayerPrefs.HasKey("MaxScore"))
        {
            PlayerPrefs.SetInt("MaxScore", 0);
        }
        m_ScoreText.text = "Score : " + m_Score.ToString();
        m_SpeedText.text = "Speed : " + m_MoveSpeed.ToString();
        m_CurrScore = m_Score;
    }

    public void IncrementScore(int value)
    {
        m_CurrScore += value;
        m_ScoreText.text = "Score : " + m_CurrScore.ToString();
    }

    public void SetSpeed(int moveSpeed)
    {
        m_MoveSpeed = moveSpeed;
        m_SpeedText.text = "Speed : " + m_MoveSpeed.ToString();
    }

    public int GetCurrScore()
    {
        return m_CurrScore;
    }

    public void SetScore(int score)
    {
        m_Score = score;
    }
}