using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckHightScore : MonoBehaviour
{
    [SerializeField] GameObject m_HightScoreText;
    Text m_Text;

    void Start()
    {
        m_Text = m_HightScoreText.GetComponent<Text>();
    }


    public void CheckScore()
    {
        m_HightScoreText.SetActive(true);
        m_Text.text = "Hight Score : " + PlayerPrefs.GetInt("MaxScore").ToString();
    }
}
