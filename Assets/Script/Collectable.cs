using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    [SerializeField] float m_SpeedRotation;
    [SerializeField] float m_SpeedMovement;
    [SerializeField] float m_Emplitude;
    [SerializeField] int m_ScoreValue;

    AudioSource m_AudioSource;

    bool m_Down = false;

    float m_TargetUp;
    float m_TargetDown;

    void Start()
    {
        m_TargetUp = transform.position.y + (m_Emplitude / 2);
        m_TargetDown = transform.position.y - (m_Emplitude / 2);
        m_AudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        SetRotation();
        SetPosition();
    }

    void SetRotation()
    {
        transform.Rotate(0, 0, m_SpeedRotation * Time.deltaTime);
    }

    //fait bouger le collectable de bas a haut
    void SetPosition()
    {
        Vector3 position = transform.position;
        if(m_Down)
        {
            position.y -= m_SpeedMovement * Time.deltaTime;
            if(position.y <= m_TargetDown)
            {
                position.y = m_TargetDown;
                m_Down = false;
            }
        }
        else
        {
            position.y += m_SpeedMovement * Time.deltaTime;
            if (position.y >= m_TargetUp)
            {
                position.y = m_TargetUp;
                m_Down = true;
            }
        }
        transform.position = position;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<PlayerMovement>().AddScore(m_ScoreValue);
            Destroy(gameObject);
        }
    }
}
