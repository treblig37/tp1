using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField] float m_MaxRotationSpeed = 5.0f;
    [SerializeField] float m_MinRotationSpeed = 2.0f;

    float m_RotationSpeed;
    int m_Direction;

    void Start()
    {
        m_RotationSpeed = Random.Range(m_MinRotationSpeed, m_MaxRotationSpeed);
        m_Direction = Random.Range(0, 2);
    }

    void Update()
    {
        SetRotation();
    }

    //fait rotationner du sens selon la direction choisi aleatoirement
    void SetRotation()
    {
        float currentSpeed = 0.0f;
        if(m_Direction == 1)
        {
            currentSpeed = m_RotationSpeed;
        }
        else
        {
            currentSpeed = -m_RotationSpeed;
        }
        transform.localEulerAngles += new Vector3(0.0f, 0.0f, currentSpeed) * Time.deltaTime;
    }
}
