using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouce : MonoBehaviour
{
    [SerializeField] float m_BouceForce = 0;
    [SerializeField] Rigidbody m_PlayerRigidbody;

    //fait rebondire le player lorsque il rebtre en collsion en lui ajoutant une velo en y
    void OnCollisionEnter(Collision collision)
    {
        if(collision.other.CompareTag("Player"))
        {
            Vector3 velo = m_PlayerRigidbody.velocity;
            velo.y = m_BouceForce;
            m_PlayerRigidbody.velocity = velo;
        }
    }
}
