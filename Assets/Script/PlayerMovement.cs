using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float m_Speed;
    [SerializeField] Transform m_CamTransfrom;
    [SerializeField] float m_JumpForce;
    [SerializeField] float m_Exelerateur = 1;
    [SerializeField] GameManager m_GameManager;
    [SerializeField] HUD m_Hud;
    [SerializeField] AudioClip m_TakeCacaouait;
    [SerializeField] AudioClip m_TombeDansLeVide;
    [SerializeField] AudioClip m_FinishAudio;
    [SerializeField] AudioClip m_JumpAudio;
    [SerializeField] AudioClip m_DeathSong;

    AudioPlayer m_PlayMusique;


    Rigidbody m_Rigidbody;
    float m_Exelerateur1 = 1;

    bool m_ToucheSol = false;
    bool m_CanJump = false;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_PlayMusique = GetComponent<AudioPlayer>();
    }

    
    void Update()
    {
        if(!m_GameManager.CheckIsDeath())
        {
            if (m_ToucheSol)
            {
                Movement();
            }
            if(m_CanJump)
            {
                Jump();
            }
        }
        CheckDeath();
    }

    //prend les intut de la camera et les fait bouger selon la direction que la camera regarde et applique la velociter
    void Movement()
    {
        Vector3 movement = m_Rigidbody.velocity;
        movement.x = -(Input.GetAxis("Horizontal") * m_Speed * m_Exelerateur1);
        movement.z = -(Input.GetAxis("Vertical") * m_Speed * m_Exelerateur1);

        Vector3 worldDirection;
        worldDirection.x = m_CamTransfrom.TransformVector(movement).x;
        worldDirection.z = m_CamTransfrom.TransformVector(movement).z;
        worldDirection.y = movement.y;

        m_Rigidbody.velocity = worldDirection;
        m_Hud.SetSpeed((int)m_Rigidbody.velocity.magnitude);
    }

    //permet dajouter une velociter en y et permet de sauter seulement une fois si il est dans les air
    void Jump()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            m_PlayMusique.PlayerClip(m_JumpAudio);
            Vector3 jump = m_Rigidbody.velocity;
            jump.y = m_JumpForce;
            m_Rigidbody.velocity = jump;
            if(!m_ToucheSol)
            {
                m_CanJump = false;
            }
        }
    }

    void CheckDeath()
    {
        if(transform.position.y <= m_GameManager.CheckMinY() && !m_GameManager.CheckIsDeath())
        {
            m_PlayMusique.PlayerClip(m_TombeDansLeVide);
            m_GameManager.SetIsDeath(true);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if(collision.other.CompareTag("Rampe"))
        {
            m_Exelerateur1 = m_Exelerateur;
            m_ToucheSol = true;
            m_CanJump = true;
        }
        else if(collision.other.CompareTag("Sol"))
        {
            m_ToucheSol = true;
            m_CanJump = true;
        }
        if(collision.other.CompareTag("Danger") && !m_GameManager.CheckIsDeath())
        {
            m_PlayMusique.PlayerClip(m_DeathSong);
            m_GameManager.SetIsDeath(true);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.other.CompareTag("Rampe"))
        {
            m_Exelerateur1 = 1;
            m_ToucheSol = false;
        }
        else if (collision.other.CompareTag("Sol"))
        {
            m_ToucheSol = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            m_PlayMusique.PlayerClip(m_FinishAudio);
            m_GameManager.SetIsFinish(true);
        }
    }

    public void AddScore(int score)
    {
        m_PlayMusique.PlayerClip(m_TakeCacaouait);
        m_Hud.IncrementScore(score);
    }
}
