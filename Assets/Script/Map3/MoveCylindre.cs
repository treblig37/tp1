using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCylindre : MonoBehaviour
{
    [SerializeField] float m_RotationSpeed = 2.0f;
    [SerializeField] float m_MovementSpeed = 2.0f;
    [SerializeField] float m_LimitDistance = 0.0f;

    Rigidbody m_Rb;
    Vector3 m_Velo;

    void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        if(transform.position.x < m_LimitDistance)
        {
            SetRotation();
            SetMovement();
        }
    }

    void SetRotation()
    {
        transform.localEulerAngles += new Vector3(0.0f, 0.0f, m_RotationSpeed) * Time.deltaTime;
    }

    //fait avancer le game object en lui donnant une velociter
    void SetMovement()
    {
        m_Velo = m_Rb.velocity;
        m_Velo.x = m_MovementSpeed;
        m_Velo.z = 0;
        m_Rb.velocity = m_Velo;
    }
}
