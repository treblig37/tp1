﻿300+ Hand Painted Textures Mega Bundle is our best seller with a huge type of textures.

Thank you for buying this texture collection! 

We are so happy to announce that everyone who buys this texture collection will get access to following 

MEGA futures:
- Discord badge and private channel;
- 50% Discount (works on lowlypoly.com);
- 1x Free Texture Request (2 textures);
- Priority support.

Mega Bundle Texture collection series:
- Super Texture Collection (over 600+ textures) - https://bit.ly/3xVzTQd
- Mega Bundle Part 1 (300+ textures) - Hand Painted Textures - https://bit.ly/3HKHgyO
- Mega Bundle Part 2 (300+ textures) - Ultimate Texture Collection - https://bit.ly/31jTNc9
- Mega Bundle Part 3 (325+ textures) - Stylized Textures - https://bit.ly/3E6UaDU
*you can check them on Unity Asset Store , you have a 50% discount for each collection.

Review:
Your reviews for us are the best to say thank you if you are happy with this texture collection.
Your reviews motivate us to make more free updates for this bundle and help this collection be popular on Unity Asset Store.

Discord Badge!
Everyone will receive a discord badge, please join the discord and send me a pm with your invoice number, I will verify it and add you a badge.

Texture Request:
If you can't find something in this texture collection, please join our discord and add texture request in channel "request-textures"

JOIN THE LOWLYPOLY COMMUNITY:
https://discord.gg/dm4VANd

Lowlypoly , 
www.lowlypoly.com
support@lowlypoly.com

*If you want to use Mega Perks, please send us your invoice number (start from IN...) to email - support@lowlypoly.com
**You will receive an extra gift if you add a review in 1-2 days, please send a screenshot with your review to email - support@lowlypoly.com
